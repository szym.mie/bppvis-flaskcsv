try: 
    from flask import Flask, abort, make_response
    from flask_cors import CORS, cross_origin
except ModuleNotFoundError as err:
    print(f"{err} - please install this module.")
    exit(1)

app = Flask(__name__)
cors = CORS(app)
app.config["CORS_HEADERS"] = "Content-Type"

@app.route("/partnership/<int:fid>")
@cross_origin()
def serve_csv(fid):
    try:
        with open(f"./dat/{fid}.csv") as fio:
            csv = fio.read()
            res = make_response(csv)
            res.headers["Content-Type"] = "text/csv"
            return res
    except FileNotFoundError:
        abort(404)
        