# bppvis-flaskcsv

## What is it?

This is a demonstrator for a ***CSV file format***, written using *Flask* framework.

Be sure to also check out parent project [bppvis](https://gitlab.com/szym.mie/adiag) - you will need that to actually see the data.

## Starting out

First **grab the *[bppvis](https://gitlab.com/szym.mie/adiag)*** project.

To use this server **you will need *Python 3***. After installing, **get the following modules: *flask*, *flask_cors***.
Please see this [*pip* quickstart](https://pip.pypa.io/en/stable/quickstart/) - you will need to use that.

You will need a server for the *bppvis* itself. Probably the easiest to install is *LiveServer* for *Visual Studio Code*.
There are also *Apache* and *nginx*, and many others.

Since *Flask* servers **prefer port 5000**, you will need to change the port of the *bppvis* server. This is server-specific, thus you will need to find a documentation on that.

There is only one step left - search the *main.js* in *bppvis* for the constant **BPP_API_URL**, and change it to:

    const BPP_API_UR = "http://127.0.0.1:5000/partnership/";

Finally, run the *Flask* server, **using *run.bat***, and then your *bppvis* server. Remember to add adequate params to your URL:

- **id** - set to existing ID in *dat* folder.
- **df** - defaults to *JSON* data format, you need to set it to "csv"

Try this parameters:

    ?id=4059,3022&df=csv

That is pretty much all you need to know to get started.

---

## Get more data

If you got your hands on a *JSON* or *DAT* files, containing real data, you can use the *dtoc.py* conversion tool from *[bppvis](https://gitlab.com/szym.mie/adiag)* repo. There is a manual how to use it, fortunately it is cross-platform and uses only built-in Python modules, so hopefully you won't have too much hassle with it.